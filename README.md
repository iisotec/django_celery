# Ejecución de tareas asíncronas con CELERY, RABBITMQ y DJANGO

Proyecto realizado en python 3

## Instalar Celery y RabbitMq 

```
$ sudo apt-get install -y erlang
```

```
$ sudo apt-get install rabbitmq-server
```

## Ejecutar el proyecto

Para ejecutar el proyecto debe de tener instalado VirtualWrapper, Python3 y pip3, y sigas las instrucciones:
Nota: si esta trabajando con Virtualenv, puede ejecutar por ejemplo: virtualenv -p python3 env_django_celery

```
$ mkvirtualenv -p python3 env_django_celery
```

```
$ git clone https://gitlab.com/iisotec/django_celery.git
```

```
$ cd django_celery
```

```
$ pip install -r requirements.txt
```

```
$ ./manage.py migrate
```

```
$ ./manage.py createsuperuser
```

```
$ ./manage.py runserver
```

## Ejecutar celery en terminal

```
$ celery -A django_celery worker -l info

```

## ENLACES

[Presentación PPT](https://docs.google.com/presentation/d/1Wnqrm5ss_YYcWQOYOIUBuQnxjZuWagpfPwgo64W7w2k/edit#slide=id.g3636fc4d24_0_8)

[Celery documentación para django](http://docs.celeryproject.org/en/latest/django/first-steps-with-django.html)

[RabbitMq](https://www.rabbitmq.com)